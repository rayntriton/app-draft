import langs from '../lang/homeLang'
import settings from '../settings'
var lang  = langs[settings.lang]
export default {
  header : {
    back : {
      text : 'b',
      backModule : undefined
    },
    tittle : {
      text : lang.tittle
    },
    menu : {
      text : 'm',
      menuModule : { name: 'commanda', type: 'onDemand' }
    },
  },
  menu : {
    menuIcons : {
    }
  },
  state : {
    
  }
}
