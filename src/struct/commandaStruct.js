import langs from '../lang/commandaLang'
import settings from '../settings'
let lang  = langs[ settings.lang ]
export default {
  header : {
    back : {
      text : 'b',
      backModule : { name: 'home', type: 'singleton' } },
    tittle : {
      text : lang.tittle },
    menu : {
      text : 'm',
      menuModule : { name: 'commandaMenu', type: 'onDemand' } } },
  progress : {
    percent : 0 },
  menuList : {
    miniHeader : {
      text : lang.menuListMiniHeader },
    dataTransit : undefined,
    items : {
      list : ['a','b'] } },
  itemsList : {
    miniHeader : {
      text : lang.itemsListMiniHeader },
    dataTransit : undefined,
    items : {
      list : [] } },
  additionalData : {
    headerFields : [ 'customer, name', 'receip, total' ],
    bodyFields : [ 'customer, address', 'customer, id', 'receipt, notes' ] },
  searchBar : {
    placeholder : lang.searchBar,
    dataTransit : undefined },
  submit : {
    text : 's',
    submitModule : { name: 'commandaSubmit', type: 'onDemand' } } }
