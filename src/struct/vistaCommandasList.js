import lang from './vistaCommandasList.lang'
export default vistaCommandasList =
{
  header : {
    id : 'header',
    back : {
      id : 'back',
      link : {
        id : 'link',
        target : 'vistaHome'
      }
    },
    tittle : {
      id : 'tittle',
      text : lang.tittle
    },
  },
  vistaCommandas : {
    id: 'vistaCommandas',
    list : []
  },
  plusOne:{
    id : 'plusOne'
  }

}
