import Header from './header'
import MenuList from './menuList'
import ItemsList from './itemsList'
import uid from '../help/uid'
import Log from '../help/log'
/*import AdditionalData from './additionalData'
*/
import SearchBar from './atoms/searchBar'
import React, {useState, useMemo} from 'react'
/*import Submit from './submit'
*/
import './vistaHome.css'
let l = new Log('VistaHome 1').log
export default function VistaHome(props){
  //const [display, setDisplay] = useState('flex')
  //useMemo( () => { props.setDisplay( ( value ) => { setDisplay( value ) } ) }, [] )
  /*useMemo( () => { props.transit( ( data ) => {
    if(data.command == 'setDisplay' && data.body.display != display ){
        setDisplay( data.body.display )
    } } ) }, [] )*/
  return (
    <div className = 'vistaHome' id = { props.id } >
      <div id = 'header' >
        <Header { ...{ ...props.inherit( props.header ), backModule: { name: 'home' }, menuModule: { name: 'commanda', parent: props.id, type: 'onDemand' } } } />
      </div>
      {/*<div id = 'menuList'>
        <MenuList {...props.menuList}/>
      </div>*/}
    </div>
  )
}
