//import './searchBar.css'
export default function SearchBar( props ){
  console.log( 'SearchBar: props = ', props )
  function search( e ){
    let results=props.module.search( e.target.value )
    props.module.menuListTrans( results )
  }
  return (
    <div className = 'searchBar' >
      <input id='searchBar' type='search' placeholder={ props.placeholder } onChange={ search } />
    </div>
  )
}
