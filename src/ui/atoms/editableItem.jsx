export default function EditableItem(props){
  function onClick(e){
    props.onClick()
  }
  return (
    <div className = 'item' onClick = {onClick}>
      <input value={props.description}/>
      <button ></button>
    </div>
  )
}
