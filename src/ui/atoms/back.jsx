import React from 'react'

import './back.css'

export default function Back(props){

  return (

    <div className = 'back'>
      <button id = 'back'> {props.text}  </button>
    </div>
  )
}
