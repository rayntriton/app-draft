/*import SearchList from './searchList'
import ItemsList from './itemsList'
import AdditionalData from './additionalData'
import SearchBar from './searchBar'
import Submit from './submit'
*/
//import './miniHeader.css'
import React from 'react'
export default function MiniHeader(props){
  return (
    <div className = 'miniHeader'>
      <p>{props.text}</p>
    </div>
  )
}
