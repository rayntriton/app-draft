//import React from 'react'

//import './item.css'

export default function Item(props){
  function onClick(e){
    props.onClick()
  }
  return (
    <div className = 'item' onClick = {onClick}>
      <p> {props.description}  </p>
    </div>
  )
}
