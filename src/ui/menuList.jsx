import MiniHeader from './atoms/miniHeader'
import Item from './atoms/item'

import React, {useState, useMemo} from 'react'
import Teleport from '../help/teleport'
/*import SearchList from './searchList'
import ItemsList from './itemsList'
import AdditionalData from './additionalData'
import SearchBar from './searchBar'
import Submit from './submit'
*/
//import './searchList.css'


export default function MenuList(props){
  console.info('MenuList: props =', props )
  const [menuContent, setMenuContent] = useState(props.items.list)
  const subscription = useMemo(()=>{
    props.module.menuListTrans( ( data ) => {
        console.info('MenuList: recieving [data]', data )
        setMenuContent(data)})},[])
  return (
    <div className = 'menuList'>
      <div id = 'miniHeader'>
        <MiniHeader {...props.miniHeader}/>
      </div>
      <div id = 'items'>
        {menuContent.map((item,i) => {
          function onclick(e){
            console.log(`clicked ${item}`)
            props.module.itemsListTrans( item )
          }
          return (
            <div key = {`item${i}`}>
              <Item description={item} onClick={onclick}/>
            </div>
          )
        })}
      </div>
    </div>
  )
}
