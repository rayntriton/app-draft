import Back from './atoms/back'
import Tittle from './atoms/tittle'
import Menu from './atoms/menu'
import React from 'react'
import Transit from '../help/transit'
import uid from '../help/uid'
//import Progress from './progress'
import './header.css'

export default function Header(props){
  function onClickBack(){
    console.log('Header: backModule = ', props.back.backModule)
    props.module.goToModule(props.back.backModule)
  }
  function onClickMenu(){
    console.log('Header: menuModule = ', props.menu.menuModule)
    props.module.goToModule(props.menu.menuModule)
  }
  return (
    <div className = 'header'>
      <button id = 'back' className = 'back' onClick={onClickBack}>
        { props.back.text }
      </button>
      <div id = 'tittle'>
        <Tittle {...props.tittle}/>
      </div>
      <button id = 'menu' className='menu' onClick={onClickMenu}>
        { props.menu.text }
      </button>
    </div>
  )
}
