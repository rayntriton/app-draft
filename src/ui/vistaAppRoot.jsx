import React, {useState, useMemo, useEffect} from 'react'
import uid from '../help/uid'
import useAb from './hooks/useAb'
import Log from '../help/log'
import './vistaAppRoot.css'
let l = new Log('VistaAppRoot 1').log

export default function VistaAppRoot( props ) {
  let state = props.state
  //const [onModulesChange, setOnModulesChange] = useState(true)
  const [ modules, setModules ] = useAb( props.modules )
  const [ activeModule, setActiveModule ] = useAb( props.activeModule )
  let getModules = modules
  log( ...l( `VistaAppRoot: rendering... activeModule=${ activeModule() }, modules=`, modules() ) )
  useMemo( () => {
    props.updateModules( ( activeModule ) => {
      setActiveModule( activeModule ) }, getModules,
    ( newModule ) => {
      setModules( {...modules(), [ newModule.id ]: newModule } ) } ) } , [] )
  if( ! state.isLogged ){
    log( ...l( `VistaAppRoot: not logged` ) )
  }
  else {
    log( ...l( 'VistaAppRoot: modules.length', Object.keys( modules() ).length ) )
    if( Object.keys( modules() ).length == 0 ){
      log( ...l( `VistaAppRoot: sending transit`, { name: 'home', parent: props.id, new: true } ) )
      props.goToModule( { name: 'home', parent: props.id, type: 'singleton' } )
    }
  }
  return (
    <div className = "appRoot" id = { props.id } >
      { Object.keys( modules() ).map( ( moduleId ) => {
        log( ...l( 'VistaAppRoot: rendering modules', modules() ) )
        log( ...l( `VistaAppRoot: moduleId[ ${ moduleId } ] == activeModule[ ${ activeModule() } ]?`) )
        let display = moduleId == activeModule() ? 'flex' : 'none'
        return (
          <div id = { moduleId } key = { moduleId } style = { { display: display } } >
            { modules()[ moduleId ].ui() }
          </div>
        ) } ) }
    </div>
  )
}
