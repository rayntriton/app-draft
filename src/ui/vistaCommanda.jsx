import Header from './header'
import MenuList from './menuList'
import ItemsList from './itemsList'
/*import AdditionalData from './additionalData'
*/
import SearchBar from './atoms/searchBar'
import React, {useState, useMemo} from 'react'
/*import Submit from './submit'
*/
import './vistaCommanda.css'

export default function VistaCommanda( props ){
  //const [ display , setDisplay ] = useState( 'flex' )
  //useMemo( () => { props.setDisplay( ( value ) => { setDisplay( value ) } ) }, [] )
  /*const [display, setDisplay] = useState('flex')
  useMemo(()=>{props.transit((data)=>{
    if(data.target=='commanda'){
      if(data.content.command=='setDisplay'){
        if(data.content.body.display!=display)
          setDisplay(data.content.body.display)
      }
    }
  })},[])*/
  return (
    <div className = 'vistaCommanda' id = { props.id } key = { props.id } >
      <div id = 'header'>
        <Header { ...props.inherit( props.header ) } />
      </div>
      <div id = 'menuList'>
        <MenuList { ...props.inherit( props.menuList ) } />
      </div>
      <div id = 'itemsList'>
        <ItemsList { ...props.inherit( props.itemsList ) } />
      </div>
      {/*<div id = 'additionalData'>
        <AdditionalData {...props.additionalData}/>
      </div>*/}
      <div id = 'searchBar'>
        <SearchBar { ...props.inherit( props.searchBar ) } />
      </div>
      {/*<div id = 'submit'>
        <Submit {...props.submit}/>
      </div>*/}
    </div>
  )
}
