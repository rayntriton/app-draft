export default function VistaEditItem(props){
  return (
    <div className = 'vistaEditItem' id = {props.id}>
      <div id = 'header'>
        <Header {...props.header}/>
      </div>
      <div id = 'editableItem'>
        <EditableItem {...props.editableItem}/>
      </div>
    </div>
  )
}
