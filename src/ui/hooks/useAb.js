import { useMemo, useState } from 'react'
export default function useAb( ab ){
  let rerender = useState( 0 )[ 1 ]
  let abMemo = useMemo( () => { return { state: ab } }, [] )
  let get = () => abMemo.state
  let set = ( newAb ) => {
    if( typeof newAb == 'function' ){
      abMemo.state = newAb( get() )
    }
    else abMemo.state = newAb
    rerender( i => ++ i )
  }
  return [ get, set ]
}
