import MiniHeader from './atoms/miniHeader'
import Item from './atoms/item'
import uid from '../help/uid'
import React, {useState, useMemo} from 'react'

export default function ItemsList(props){
  const [ listContent, setListContent ] = useState( props.items.list )
  props.items.list = listContent
  useMemo( () => { props.module.itemsListTrans( ( data ) => { setListContent( [ ...props.items.list, data ] ) } ) } , [] )
  /*useMemo(()=>{
    props.module.transit((data)=>{
      if(data.command=='ItemsList'){
        console.info('ItemsList: recieving data', data, 'adding to', listContent)
        setListContent([...props.items.list, data.body])
      }
  })},[])*/
  return (
    <div className = 'itemsList'>
      <div id = 'miniHeader'>
        <MiniHeader {...props.miniHeader}/>
      </div>
      <div key = 'items'>
        {listContent.map((item,i) => {
          function edit(item){
            console.log('editing')
            //props.module.transit({command: 'editItem', body: item})
          }
          return (
            <div key = {`item${i}`}>
              <Item description={item} onClick={(e)=>{let item_ = item; edit(item_)}}/>
            </div>
          )
        })}
      </div>
    </div>
  )
}
