import randomString from '../help/randomString'
export default function itemsList(n = 5){
  let results = []
  for(let i = 0; i < n; i++ ) results.push(randomString(10,10,3,5))
  return results
}
