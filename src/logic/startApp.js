import React from 'react'
import ReactDOM from 'react-dom'
import CommandaConstruct from '../construct/commandaConstruct'
import AppRootConstruct from '../construct/appRootConstruct'
import uid from '../help/uid'
import Log from '../help/log'
let log1 = new Log('startApp 1').log
global.log = console.log

export default function startApp(){
  Log.level = /1/
  let id = uid()
  let parent = id
  let name = 'appRoot'
  log1('startApp: starting...')
  let root = new AppRootConstruct({id: id, name: name, parent: parent})

  ReactDOM.render(
    <React.StrictMode>
      {root.ui()}
    </React.StrictMode>
    ,
    document.getElementById('root')
  )
}
