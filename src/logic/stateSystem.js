function stateSystem( componentUnit ){
  stateSystem.status = 'processing'
  stateSystem.registry[ componentUnit.id ] = componentUnit
  stateSystem.componentRunningId = componentUnit.id
  componentUnit.count = 0
  componentUnit.isQueued = false
  componentUnit.component( componentUnit.args )
  componentUnit.alreadyInitialized = true
  stateSystem.componentRunningId = null
  if( stateSystem.queue[ 0 ] ){
    nextComponentUnit = stateSystem.queue.shift()
    stateSystem( nextComponentUnit )
  }
  else {
    stateSystem.status = 'idle'
  }
}
stateSystem.status = 'idle'
stateSystem.queue = []
stateSystem.queueSys = ( component, args ) => {
  stateSystem.queueSys_( new ComponentUnit( component, args ) )
}
stateSystem.queueSys_ = ( componentUnit ) => {
  if(stateSystem.status == 'idle' ){
    stateSystem( componentUnit )
  }
  else stateSystem.queue.push( componentUnit )
}
stateSystem.count = { next: () => ++ stateSystem.count.value, current: () => stateSystem.count.value, value: 0 }
stateSystem.registry = {}
function state( thing ){
  if( ! stateSystem.componentRunningId ) throw new Error('cant call outside of components')
  let componentUnit = stateSystem.registry[ stateSystem.componentRunningId ]
  const index = componentUnit.count ++
  //log(componentUnit)
  if(componentUnit.alreadyInitialized){
    return [ componentUnit.getState[ index ], componentUnit.setState[ index ] ]
  }
  else {
    componentUnit.state[ index ] = thing
  }
  function get(){
    //log( 'index =', index )
    return componentUnit.state[ index ]
  }
  componentUnit.getState[ index ] = get
  function set( thing ){
    componentUnit.state[ index ] = thing
    if( ! componentUnit.isQueued ){
      stateSystem.queueSys_(componentUnit)
      componentUnit.isQueued = true
    }
  }
  componentUnit.setState[ index ] = set
  componentUnit.set=set
  return [ get, set ]
}
function ComponentUnit( component, args ){
  this.id = StateSystem.count.next()
  this.component = component
  this.args = args
  this.state = []
  this.setState = []
  this.getState = []
  this.count = 0
  this.alreadyInitialized = false
  this.isQueued = false
}
let component = stateSystem.queueSys
export {component as default, state}
