import AppRootConstruct from '../construct/appRootConstruct'
import appRootStruct from '../struct/appRootStruct'
import VistaAppRoot from '../ui/vistaAppRoot'

import HomeConstruct from '../construct/homeConstruct'
import homeStruct from '../struct/homeStruct'
import VistaHome from '../ui/vistaHome'

import CommandaConstruct from '../construct/commandaConstruct'
import commandaStruct from '../struct/commandaStruct'
import VistaCommanda from '../ui/vistaCommanda'

export default {
  appRoot : {
    name : 'appRoot',
    struct : appRootStruct,
    construct : AppRootConstruct,
    vista : VistaAppRoot
  },
  home : {
    name : 'home',
    struct : homeStruct,
    construct : HomeConstruct,
    vista : VistaHome
  },
  commanda : {
    name : 'commanda',
    struct : commandaStruct,
    construct : CommandaConstruct,
    vista : VistaCommanda
  }

}
