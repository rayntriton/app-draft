import Construct from './construct'
import homeStruct from '../struct/homeStruct'
export default function HomeConstruct( props ){
  Object.assign( this, new Construct( this, homeStruct, props ) )

}
