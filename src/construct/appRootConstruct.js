import React from 'react'
import ReactDOM from 'react-dom'
import VistaAppRoot from '../ui/vistaAppRoot'
import appRootStruct from '../struct/appRootStruct'
import Transit from '../help/transit'
import construct from './construct'
import uid from '../help/uid'
import modulesConfig from '../logic/modulesConfig'
import Log from '../help/log'
let log1 = new Log('AppRootConstruct 1').log

function AppRootConstruct( props ){
  window.root = this
  log1( 'AppRootConstruct: starting...' )
  //Object.assign( this, appRootStruct )
  Object.assign( this, construct( this, appRootStruct, props ) )
  let this_ = this
  let state = this.state
  state.lastActiveModule = null //this.id
  state.activeModule = this.id
  /** module:
    { name: 'moduleName' [, parent: 'parentUid'] [, id: 'uniqueId'] }
  */
  this.updateModules = ( setActiveModule, getModules, setModules) => {
    log1( 'AppRootConstruct: SUBSCRIBING to goToModule' )
    log1( 'subscounter', window.subscounter )
  this.goToModule( ( module ) => {
    let newModule
    let activeModule
    if( ! module.name ) throw new Error(`AppRootConstruct.updateModules: module need a name. module = ${JSON.stringify(module)}`)
    if( module.type == 'onDemand' ){
      let Construct = modulesConfig[ module.name ].construct
      activeModule = module.id ? module.id : uid()
      newModule = new Construct( { ...module, id: activeModule } )
    }
    else if( module.type == 'singleton' ){
      if( module.id ){
        if( state.modules[ module.id ] )
          activeModule = module.id
        else {
          let Construct = modulesConfig[ module.name ].construct
          newModule = new Construct( module )
          activeModule = module.id
        }
      }
      else {
        var matchedIds = []
        for( var id in getModules() )
          if( getModules()[ id ].name == module.name ) matchedIds.push( id )
        if( matchedIds.length == 0 ) {
          console.log( 'AppRootConstruct.goToModule zero matches', module )
          let Construct = modulesConfig[ module.name ].construct
          activeModule = module.id ? module.id : uid()
          newModule = new Construct( { ...module, id: activeModule } )

        }
        else if( matchedIds.length == 1 ) activeModule = matchedIds[ 0 ]
        else throw new Error( `Too many modules found for ${ JSON.stringify( module ) }` )
      }
    }
    if( newModule ) {
      setModules( newModule )
      //state.modules[ newModule.id ] = newModule
    }
    if( activeModule ) {
      setActiveModule( activeModule )
      //state.activeModule = activeModule
    }
    //if( activeModule || newModule) setOnModulesChange()
  } ) }
  console.log( 'AppRootConstruct: finishing construct', this )
  this.ui = () => {
    return (
      <React.Fragment>
        <VistaAppRoot { ...this_ }/>
      </React.Fragment>
    )
  }
}
let transit = AppRootConstruct.transit
export {AppRootConstruct as default, transit}
