import modulesConfig from '../logic/modulesConfig'
import Transit from '../help/transit'
import React from 'react'
import Log from '../help/log'
let l = new Log('construct 1').log
/**
  modulet:
  { id: 'uniqueId' , name: 'moduleName', parent: 'parentId' }
*/
let subscounter = {counter: 0}
window.subscounter = subscounter
export default function construct( this_, struct = {}, modulet ){
  Object.assign( this_, struct, modulet )
  log(...l( 'construct: modulet=', modulet ))
  let transitWay = function( id, target, command ){
    return ( msg ) => {
      if( typeof msg == 'function'){
        subscounter.counter++
        log(...l('construct.transitWay.annonymous constructing new subscription', msg))
        return new Transit( id ).send( ( data ) => { if( data.target == target && data.content.command == command ) msg( data.content.body ) } )
      }
      else {
        return new Transit( id ).send( { target: target, content: { command: command, body: msg } } )
      } } }
  this_.transitWay = transitWay
  this_.transitRoot = transitWay( 'root', 'appRoot', 'transitRoot' )
  let transitCommand = (command) => transitWay( this_.id, this_.name, command )
  this_.transitCommand = transitCommand
  this_.display = transitCommand('display')
  this_.goToModule = transitWay( 'root', 'appRoot', 'goToModule')
  this_.inherit = ( props ) => { return { ...props, module: this_, inherit: this_.inherit } }
  let Vista = modulesConfig[ modulet.name ].vista
  this_.ui = function(){
    return (
      <React.Fragment>
        <Vista { ...this_ } />
      </React.Fragment>
    ) }
}
