import commandaStruct from '../struct/commandaStruct'
import Construct from './construct'
import VistaCommanda from '../ui/vistaCommanda'
import ObjectIndex from '../help/objectIndex'
import Transit from '../help/transit'
import uid from '../help/uid'
import React from 'react'
import ReactDOM from 'react-dom'
import getItemsList from '../devSources/genItemsList'
export default function CommandaConstruct(props){
  console.log('CommandaConstruct: starting...')
  //var oi = new ObjectIndex({...commandaStruct})
  Object.assign( this, new Construct( this, commandaStruct, props ) )
  let this_ = this
  this.search = ( input ) => {
    let results = getItemsList( 10 )
    let resultsf = results.filter( str => str.match( input ) )//.split(' ');
    //console.log(`searching '${input}', resultsf are '${resultsf}'`)
    return resultsf
  }
  this.menuListTrans = this.transitCommand('menuList')
  this.itemsListTrans = this.transitCommand('itemsList')
  //let transit = (id) => {return new Transit(id).send}
  //this.transit = transit(this_.id)
  //oi.set('menuList, dataTransit',{transit: transit(this_.id)})
  //oi.set('itemsList, dataTransit',{transit: transit(this_.id)})
  /*oi.set( 'searchBar, dataTransit', {
    //transit: transit(this_.id),
    search: function( input ){
      let results = getItemsList( 10 )
      let resultsf = results.filter( str => str.match( input ) )//.split(' ');
      //console.log(`searching '${input}', resultsf are '${resultsf}'`)
      return resultsf
    } } )*/
  //console.log( oi.getObject() )
  //Object.assign(this, { ...oi.getObject() } )
}
