function typeOf(value) {
  var s = typeof value;
  if (s === 'object') {
    if (value) {
      if (value instanceof Array) s = 'array'
    }
    else s = 'null';
  }
  return s;
}

function jPath(tree){
  var result = []
  const reducer = (p, c) => p[c];
  function loop(value, root=[]){
    //log('result',result)
    //log('root,value\t\t',root,value)
    if(typeof value != 'object') { result.push(root); return }
    if(typeOf(value) == 'object'){
      if(root.length != 0) result.push(root)
      Object.keys(value).map((key,index) => {
        let newRoot=[...root]
        newRoot.push(key)
        //log(key,index,newRoot,value)
        let newValue = newRoot.reduce(reducer,tree)
        //log('newRoot,newValue\t',newRoot,newValue)
        loop(newValue,newRoot)
      })
    }
    else if(typeOf(value) == 'array'){
      if(root.length != 0) result.push(root)
      value.map((key,index) => {
        const newRoot=[...root]
        newRoot.push(index)
        //log(key,index,value,newRoot)
        const newValue = newRoot.reduce(reducer,tree)
        loop(newValue,newRoot)
      })
    }
  }
  loop(tree)
  return result
}

function branches(tree, bleaf){
    var kl = bleaf.length,
        leaf = bleaf[kl - 1]
    return tree.filter(path=>{
    //log(path)
    var pl = path.length,
        node = path[pl - 1],
        pattern=bleaf.join('[^]*')
    //log('lastKey, lastNode',leaf, node)
    //log(path.join(' '),'.match(',pattern,') =',path.join(' ').match(new RegExp(pattern)))
    if(leaf == node && path.join(' ').match(new RegExp(pattern))){
        //log(path)
        return true
    }
})}

function setLeaf(tree, path, value, method){
    const reducer = (p, c) => p[c];
    var prePointer = path.slice(0,path.length-1)
                .reduce(reducer,tree),
        leaf = path.slice(path.length-1)
    if(typeOf(prePointer[leaf])=='array'){
        if(method =='unshift' || method == 'top')
           prePointer[leaf].unshift(value)
        else if(method =='push' || method == 'bottom')
           prePointer[leaf].push(value)
        else if(method =='shift' || method == 'untop')
           prePointer[leaf].shift(value)
        else if(method =='pop' || method == 'unbottom')
           prePointer[leaf].pop(value)
        else if(method =='replace')
           prePointer[leaf] = value
        else {
            console.error(new Error(`method not known ${method}`))
            return false
        }
    }
    else if(typeOf(prePointer[leaf])=='object'){
        if(method =='assign' || method == 'append')
           Object.assign(prePointer[leaf], value)
        else if(method =='replace')
           prePointer[leaf] = value
        else {
            console.error(new Error(`method not known '${method}'`))
            return false
        }
    }
    else {
        prePointer[leaf] = value
    }
    return true
}

function isLeaf(tree,path){
    var o = tree
    for(const p of path){
        o=o[p]
        if(!o) return false
    }
    return true
}

function ObjectIndex(object){
  const reducer = (p, c) => p[c];
  this.object=object
  this.paths=jPath(object)
  this.cache={}
  var this_=this
  this.get = function(path){
    var path_ = path.replace(/^[\s\.,]+|[\s]+|[\s\.,]+$/g,'')
    if(this_.cache[path_]){
      if(isLeaf(this_.object, this_.cache[path_])){
        //log('CACHE')
        return this_.cache[path_].reduce(reducer,this_.object)
      }
      else{
        this_.paths=jPath(this_.object)
        delete this_.cache[path_]
      }
    }
    var keys = path_.split(/[,\.]/g),
        matchedPaths = branches(this_.paths,keys)
    if(matchedPaths.length!=1) return undefined
    else {
      this_.cache[path_] = matchedPaths[0]
      return matchedPaths[0].reduce(reducer,this_.object)
    }
  }
  this.set = function(path, value, method='replace'){
    var path_ = path.replace(/^[\s\.,]+|[\s]+|[\s\.,]+$/g,'')
    if(this_.cache[path_]){
      if(isLeaf(this_.object, this_.cache[path_])){
        var cachePath = this_.cache[path],
            prePointer = cachePath.slice(0,cachePath.length-1)
                            .reduce(reducer,this_.object)
        if(setLeaf(this_.object, cachePath, value, method)){
          return true
        }
        else return false
      }
      else{
        this_.paths=jPath(this_.object)
        delete this_.cache[path_]
      }
    }
    var keys = path_.split(/[,\.]/g),
        matchedPaths = branches(this_.paths,keys)
    if(matchedPaths.length!=1) return false
    else {
        var prePointer = matchedPaths[0].slice(0,matchedPaths[0].length-1)
            .reduce(reducer,this_.object)
        if(setLeaf(this_.object, matchedPaths[0], value, method)){
            this_.cache[path_] = matchedPaths[0]
            this_.paths=jPath(this_.object)
            return true
        }
        else return false
    }
  }
  this.getObject = function(){return this_.object}
}
export default ObjectIndex
