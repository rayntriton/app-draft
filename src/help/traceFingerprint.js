import hashCode from './hashCode'
export default function traceFingerprint(call){
  let fingerprint = call.toString()
    +(new Error)
    .stack.split(/\r?\n/)
    .map((l, i) => {if(i > 5) return ''; return l})
    .join(' -|- ')
  return hashCode(fingerprint)
}
