import hashCode from './hashCode'
export default function Transit( ...channels ){
  let this_ = this
  let channel = channels.join( '/' )
  if( ! Transit.channels[ channel ] ) Transit.channels[ channel ] = {}

  this.channel = channel
  this.id = Transit.counter ++
  Transit.channels[ channel ][ this.id ] = this
  this.send = function( thing ){
    //console.log((new Error).stack.split(/\r?\n/))
    if( typeof thing == 'function' ){
      let functionId = hashCode( thing.toString() )
      this_.functionId = functionId
      if( ! Transit.channels[ channel ][ this_.id ][ 'subscriptions' ] ){
        console.info( 'Transit: start subscriptions field for', thing, 'functionId', functionId )
        Transit.channels[ channel ][ this_.id ][ 'subscriptions' ] = {}
      }
      if( ! Transit.channels[ channel ][ this_.id ][ 'subscriptions' ][ functionId ] )
        console.info( `Transit[ ${ this_.channel } ]: new subscription for`, thing.toString(), 'functionId', functionId )
      else
        console.info( `Transit[ ${ this_.channel } ]: replacing subscription for`, thing.toString(), 'functionId', functionId )
      Transit.channels[ channel ][ this_.id ][ 'subscriptions' ][ functionId ] = thing
    }
    else if( '</unsubscribe>' == thing ){
      console.info( `Transit[ ${ this_.channel } ]: deleting subscription`, thing.toString(), 'from', this_ )
      delete Transit.channels[ channel ][ this_.id ][ 'subscriptions' ][ this_.functionId ]
    }
    else {
      Object.keys( Transit.channels[ channel ] ).map( ( key1 ) => {
        if( Transit.channels[ channel ][ key1 ][ 'subscriptions' ] )
          Object.keys( Transit.channels[ channel ][ key1 ][ 'subscriptions' ] ).map( ( key2 ) => {
            if( ! ( thing.target && thing.content && thing.content.command && thing.content.body ) )
              throw new Error( `Transit[ ${ this_.channel } ]: Bad message ${ JSON.stringify( thing ) }` )
            console.info( `Transit[ ${ this_.channel } ]: sending data`, thing, 'functionId', key2 )
            Transit.channels[ channel ][ key1 ][ 'subscriptions'][ key2 ]( thing )
          })
        else console.info( `Transit[ ${ this_.channel } ]: no subscriptions, message goes to blackhole`, thing )
      })
    }
  }
}
Transit.counter = 0 //Number.MIN_SAFE_INTEGER
Transit.channels = {}
window.Transit = Transit
