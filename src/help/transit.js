import hashCode from './hashCode'
export default function Transit( ...channels ){
  let this_ = this
  let channel = channels.join( '/' )
  if( Transit.channels[ channel ] ) return Transit.channels[ channel ]
  this.channel = channel
  this.id = Transit.counter ++
  Transit.channels[ channel ] = this
  this.bye = function( subscriptionId ){
    delete Transit.channels[ this_.channel ][ subscriptionId ]
  }
  this.send = function( thing ){
    //console.log((new Error).stack.split(/\r?\n/))
    if( typeof thing == 'function' ){
      let functionId = hashCode( thing.toString() )
      if( ! Transit.channels[ channel ][ 'subscriptions' ] ){
        console.info( 'Transit: start subscriptions field for', thing, 'functionId', functionId )

        Transit.channels[ channel ][ 'subscriptions' ] = {}
        Transit.channels[ channel ].subscriptionsCount = 0
        Transit.channels[ channel ].subscriptionId = () => {
          console.log(`Transit: subscriptionsCount = `, Transit.channels[ channel ].subscriptionsCount )
          return Transit.channels[ channel ].subscriptionsCount ++
        }

      }
      let subscriptionId = Transit.channels[ channel ].subscriptionId()
      console.info( `Transit[ ${ this_.channel } ]: new subscription for`, thing.toString(), 'subscriptionId', subscriptionId )
      console.info( 'Transit: Transit before sub', Object.keys( Transit.channels[channel].subscriptions ).length, Transit.channels[channel].subscriptions['0'], Transit.channels )
      Transit.channels[ channel ][ 'subscriptions' ][ subscriptionId ] = { exe: thing, src: thing.toString() }//, stack: new Error().stack }
      console.info( 'Transit: Transit after sub', Object.keys( Transit.channels[channel].subscriptions ).length, Transit.channels[channel].subscriptions['0'] )
      return subscriptionId
    }
    else {
      if( Object.keys( Transit.channels[ channel ][ 'subscriptions' ] ).length > 0 )
        Object.keys( Transit.channels[ channel ][ 'subscriptions' ] ).map( ( subscriptionId ) => {
          if( ! ( thing.target && thing.content && thing.content.command && thing.content.body ) )
            throw new Error( `Transit[ ${ this_.channel } ]: Bad message ${ JSON.stringify( thing ) }` )
          console.info( `Transit[ ${ this_.channel } ]: sending data`, thing, ' to subscriptionId', subscriptionId.toString() )
          Transit.channels[ channel ][ 'subscriptions'][ subscriptionId ].exe( thing )
        })
      else console.info( `Transit[ ${ this_.channel } ]: no subscriptions, message goes to blackhole`, thing )
    }
  }
}
Transit.counter = 0 //Number.MIN_SAFE_INTEGER
Transit.channels = {}
window.Transit = Transit
