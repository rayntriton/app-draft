export default function Log( levels ){
  let this_=this
  this.levels = levels.split(' ')
  this.log = function( ...things ){
    let pass
    for( var i in this_.levels ){
      if( this_.levels[i].match( Log.level ) ){
        pass = true
        break
      }
    }
    if( pass ) return things
    return ''
  }
}
Log.level = /./
