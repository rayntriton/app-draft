/*
* @type
* o-<o, o>-<o: one to one, one way and two way respectively
* o-<oo, o>-<oo: one to many, one way and two way respectively
* oo-<o, oo>-<o: many to one, one way and two way respectively
* oo-<oo, oo>-<oo: many to many, one way and two way respectively
*/
export default function Teleport(type){
  this.content = undefined
  this.subscribers = []
  this.finalInitPointTaken = false
  this.finalEndPointTaken = false
  this.oneWay = ! />-/.test(type)
  this.oneInit = ! /^oo/.test(type)
  this.oneEnd = ! /oo$/.test(type)

  let this_=this
  this.getInitPoint = function(){
    if(!this.finalInitPointTaken){
      if(this.oneInit) this.finalInitPointTaken = true
      return (content) => {
        this_.content = content
        for(var subscriber of this_.subscribers)
          subscriber(content)
      }
    }
    else return null; //throw new Error('Teleport initial point allready taken')
  }
  this.getEndPoint = function(){
    if(!this.finalEndPointTaken){
      if(this.oneEnd) this.finalEndPointTaken = true
      return (subscriber) => {
        this_.subscribers.push(subscriber)
      }
    }
    else return null; //throw new Error('Teleport end point allready taken')
  }
}
Teleport.registry = {}
Teleport.newInitPoint = function (id, type) {
  if(!type)type='oo>-<oo'
  if(!this.registry[id]){
    this.registry[id] = new Teleport(type)
  }
  return this.registry[id].getInitPoint()
}
Teleport.newEndPoint = function (id, type) {
  if(!type)type='oo>-<oo'
  if(!this.registry[id]){
    this.registry[id] = new Teleport(type)
  }
  return this.registry[id].getEndPoint()
}
Teleport.send = function(id,thing){
  if(typeof thing == 'function'){
    Teleport.newEndPoint(id)(thing)
  }
  else Teleport.newInitPoint(id)(thing)
}
