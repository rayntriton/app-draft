export default function randomString(minWords=1,maxWords=1, minLength=2, maxLength=10, allow='aA0'){
    var text = ''
    var possible=''
    var low = 'abcdefghijklmnopqrstuvwxyz'
    var upp = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    var num = '0123456789'
    if(allow.search('a')>=0)possible+=low
    if(allow.search('A')>=0)possible+=upp
    if(allow.search('0')>=0)possible+=num
    var w=minWords+Math.floor(Math.random() * (maxWords-minWords))
    for( var i=0; i < w; i++ ){
        var l=minLength+Math.floor(Math.random() * (maxLength-minLength))
        text+=i>0?' ':''
        for( var j=0; j < l; j++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    return text
}
